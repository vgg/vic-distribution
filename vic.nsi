;; Copyright (C) 2021 University of Oxford
;;
;; Permission is hereby granted, free of charge, to any person obtaining
;; a copy of this software and associated documentation files (the
;; "Software"), to deal in the Software without restriction, including
;; without limitation the rights to use, copy, modify, merge, publish,
;; distribute, sublicense, and/or sell copies of the Software, and to
;; permit persons to whom the Software is furnished to do so, subject to
;; the following conditions:
;;
;; The above copyright notice and this permission notice shall be
;; included in all copies or substantial portions of the Software.
;;
;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;; EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;; MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
;; NONINFRINGEMENT. IN NO EVENT SHALL THE UNIVERSITY OF OXFORD BE
;; LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
;; ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
;; CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
;; SOFTWARE.
;;
;; Except as contained in this notice, the name of the University of
;; Oxford shall not be used in advertising or otherwise to promote the
;; sale, use or other dealings in this Software without prior written
;; authorization from the University of Oxford.

;; NSIS script to create the VIC installer.

!pragma warning error all

Unicode true
CRCCheck on

SetCompressor "lzma"
;;SetCompress off


Name "VIC (VGG Image Classification)"
Icon "images/image-search-logo.ico"
UninstallIcon "images/image-search-logo.ico"
BrandingText " "
OutFile "install-vic.exe"

InstallDir "$PROGRAMFILES64\VIC"

RequestExecutionLevel admin

Page license
Page directory
Page instfiles

UninstPage uninstConfirm
UninstPage instfiles

;; This is not an EULA, there's nothing to agree, we just need to
;; include the text.  The default caption would be "License Agreement"
;; which we also remove.
LicenseForceSelection off
LicenseText "VIC is Free and Open Source Software" "OK"
LicenseData "COPYING"
SubCaption 0 " "

Section "Install"
  SetOutPath "$INSTDIR"
  File /r "dist\*.*"
  SetOutPath "$INSTDIR"
  WriteUninstaller "$INSTDIR\uninstall-vic.exe"
SectionEnd

Section "Uninstall"
  RMDir /r "$INSTDIR\*.*"
  RMDir "$INSTDIR"
SectionEnd
